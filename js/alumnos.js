document.addEventListener('DOMContentLoaded', () => {
  const baseUrl = /api/;

  const mostrarTodos = async () => {
    const url = `${baseUrl}mostrartodos`;
    return await axios.get(url);
  };

  const alumnosRender = async () => {
    const table = document.getElementById('tableData');
    const studentTemplate = document.getElementById('studentTemplate');
    const fragment = document.createDocumentFragment();
    try {
      const response = await mostrarTodos();
      for (const data of response.data) {
        const clone = studentTemplate.content.cloneNode(true);
        clone.querySelector('#id').textContent = data.id;
        clone.querySelector('#matricula').textContent = data.matricula;
        clone.querySelector('#name').textContent = data.nombre;
        clone.querySelector('#adress').textContent = data.domicilio;
        clone.querySelector('#genre').textContent = data.sexo;
        clone.querySelector('#career').textContent = data.especialidad;

        clone.querySelector('.edit').addEventListener('click', () => {
          document.querySelector('#form-edit-id').value = data.id;
          document.querySelector('#form-edit-matricula').value = data.matricula;
          document.querySelector('#form-edit-nombre').value = data.nombre;
          document.querySelector('#form-edit-domicilio').value = data.domicilio;
          document.querySelector('#form-edit-sexo').value = data.sexo;
          document.querySelector('#form-edit-especialidad').value =
            data.especialidad;
          $('#editEmployeeModal').modal();
        });

        clone.querySelector('.delete').addEventListener('click', () => {
          document.querySelector('#form-delete-id').value = data.id;
          $('#deleteEmployeeModal').modal();
        });

        fragment.appendChild(clone);
      }

      table.innerHTML = '';
      table.appendChild(fragment);
    } catch (error) {
      console.log(error);
    }
  };

  const obtenerDatosAgregar = () => {
    const matricula = document.getElementById('matricula-add').value;
    const nombre = document.getElementById('nombre-add').value;
    const domicilio = document.getElementById('domicilio-add').value;
    const sexo = document.getElementById('sexo-add').value;
    const especialidad = document.getElementById('especialidad-add').value;

    const newStudent = {
      matricula,
      nombre,
      domicilio,
      sexo,
      especialidad,
    };

    return newStudent;
  };

  const agregarForm = document.getElementById('addForm');
  agregarForm.onsubmit = async (event) => {
    event.preventDefault();
    const newStudent = obtenerDatosAgregar();
    const url = `${baseUrl}insertar`;
    try {
      await axios.post(url, newStudent);
      window.location.reload();
    } catch (error) {
      if (error.response.data.includes('Duplicate entry')) {
        document.getElementById('alert-add').hidden = false;
      }
    }
  };

  const obtenerDatosEditar = () => {
    const id = document.getElementById('form-edit-id').value;
    const matricula = document.getElementById('form-edit-matricula').value;
    const nombre = document.getElementById('form-edit-nombre').value;
    const domicilio = document.getElementById('form-edit-domicilio').value;
    const sexo = document.getElementById('form-edit-sexo').value;
    const especialidad = document.getElementById(
      'form-edit-especialidad'
    ).value;

    const newData = {
      id,
      matricula,
      nombre,
      domicilio,
      sexo,
      especialidad,
    };

    return newData;
  };

  const editarForm = document.getElementById('editForm');
  editarForm.onsubmit = async (event) => {
    event.preventDefault();
    const newData = obtenerDatosEditar();
    const url = `${baseUrl}actualizar`;
    try {
      await axios.put(url, newData);
      window.location.reload();
    } catch (error) {
      console.log(error);
    }
  };

  const limpiarCampos = () => {
    document.getElementById('form-edit-matricula').value = '';
    document.getElementById('form-edit-nombre').value = '';
    document.getElementById('form-edit-domicilio').value = '';
    document.getElementById('form-edit-sexo').value = '';
    document.getElementById('form-edit-especialidad').value = '';
  };

  const btnLimpiar = document.getElementById('limpiar-campos');
  btnLimpiar.addEventListener('click', limpiarCampos);

  const obtenerDatosEliminar = () =>
    document.getElementById('form-delete-id').value;

  const eliminarForm = document.getElementById('deleteForm');
  eliminarForm.onsubmit = async (event) => {
    event.preventDefault();
    const id = obtenerDatosEliminar();
    const url = `${baseUrl}borrar/${id}`;
    try {
      await axios.delete(url);
      window.location.reload();
    } catch (error) {
      console.log(error);
    }
  };

  const obtenerDatosBuscar = () =>
    document.getElementById('form-search-id').value;

  const buscarForm = document.getElementById('searchForm');
  buscarForm.onsubmit = async (event) => {
    event.preventDefault();
    const table = document.getElementById('tableData');
    const studentTemplate = document.getElementById('studentTemplate');
    const fragment = document.createDocumentFragment();
    const matricula = obtenerDatosBuscar();
    const url = `${baseUrl}buscar/${matricula}`;
    try {
      let { data } = await axios.get(url);
      data = data[0];
      const clone = studentTemplate.content.cloneNode(true);
      clone.querySelector('#id').textContent = data.id;
      clone.querySelector('#matricula').textContent = data.matricula;
      clone.querySelector('#name').textContent = data.nombre;
      clone.querySelector('#adress').textContent = data.domicilio;
      clone.querySelector('#genre').textContent = data.sexo;
      clone.querySelector('#career').textContent = data.especialidad;

      clone.querySelector('.edit').addEventListener('click', () => {
        document.querySelector('#form-edit-id').value = data.id;
        document.querySelector('#form-edit-matricula').value = data.matricula;
        document.querySelector('#form-edit-nombre').value = data.nombre;
        document.querySelector('#form-edit-domicilio').value = data.domicilio;
        document.querySelector('#form-edit-sexo').value = data.sexo;
        document.querySelector('#form-edit-especialidad').value =
          data.especialidad;
        $('#editEmployeeModal').modal();
      });

      clone.querySelector('.delete').addEventListener('click', () => {
        document.querySelector('#form-delete-id').value = data.id;
        $('#deleteEmployeeModal').modal();
      });

      fragment.appendChild(clone);

      table.innerHTML = '';
      table.appendChild(fragment);
    } catch (error) {
      console.log(error);
      document.getElementById('alert-search').hidden = false;
    }
  };

  $('#searchEmployeeModal').on('hidden.bs.modal', function (e) {
    document.getElementById('alert-search').hidden = true;
    document.getElementById('form-search-id').value = '';
  });

  $('#addEmployeeModal').on('hidden.bs.modal', function (e) {
    document.getElementById('alert-add').hidden = true;
  });

  alumnosRender();
});
